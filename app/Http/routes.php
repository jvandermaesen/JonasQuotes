<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'QuoteController@index');
Route::get('create', 'QuoteController@create');
Route::get('edit/{id}', 'QuoteController@edit');

Route::post('createHandle','QuoteController@createHandle');
Route::get('delete/{id}','QuoteController@deleteHandle');
Route::post('edit','QuoteController@editHandle');