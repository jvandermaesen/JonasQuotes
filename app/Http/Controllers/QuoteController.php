<?php namespace App\Http\Controllers;

use App\Quote;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class QuoteController extends Controller
{

    public function index()
    {
        return view('quotes.index')->with('quotes', Quote::all());
    }

    public function createHandle(Requests\SaveQuoteRequest $request)
    {
        $quote = $request->all();
        Quote::create($quote);
        return redirect()->action('QuoteController@index');
    }

    public function edit($id)
    {
        return view('quotes.edit')->with('quote', Quote::find($id));
    }

    public function editHandle(Requests\SaveQuoteRequest $request)
    {

        $quote = $request->all();
        Quote::find(Input::get('id'))->update($quote);

        return redirect()->action('QuoteController@index');
    }

    public function deleteHandle($id)
    {
        Quote::destroy($id);
        return redirect()->action('QuoteController@index');
    }

}
