<div class="form-group">
    {!! Form::label('author') !!}
    {!! Form::text('author', null, ['class'=>'form-control']) !!}
    {!! $errors->first('author', '<p class="text-danger">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::label('quote') !!}
    {!! Form::textarea('quote', null, ['class'=>'form-control']) !!}
    {!! $errors->first('quote', '<p class="text-danger">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($submittext, ['class' => 'btn btn-primary']) !!}
</div>