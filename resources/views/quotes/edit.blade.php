@extends('layouts.main')

@section('content')

    <div class="row header">
        <div class="col-md-12">
            <h1 class="pull-left">Quotes</h1>
        </div>
    </div>
    <!-- end header -->

    <!-- content -->
    <div class="row">
        <!-- main content -->
        <div class="col-md-8 mainContent">
            {!! Form::model($quote, ['action' => 'QuoteController@editHandle']) !!}
            {!! Form::hidden('id') !!}

            @include('quotes.form', ['submittext'=>'Aanpassen'])

            {!! Form::close() !!}
        </div>
        <!-- end main content -->

        <!-- sidebar -->
        <div class="col-md-4 sidebar">
            <a href="" class="btn btn-info pull-right">Back to list</a>
        </div>
        <!-- end sidebar -->

    </div>
    <!-- end main content -->

@stop