@extends('layouts.main')

@section('content')

    <!-- header -->
    <div class="row header">
        <div class="col-md-12">
            <h1 class="pull-left">Quotes</h1>
        </div>
    </div>
    <!-- end header -->

    <!-- content -->
    <div class="row">

        <!-- main content -->
        <div class="col-md-8 mainContent">

            <!-- repeat blockquotes from database -->
            @if ($quotes->isEmpty())
                <p>No current quotes. Would you like to add some?</p>
            @else

                @foreach($quotes as $quote)
                    <blockquote>
                        <p>
                            {{$quote->quote}}
                        </p>
                        <footer>
                            As told by
                            <cite title="Source Title">{{$quote->author}}</cite>
                        </footer>
                        <div class="editLinks">
                            <a href="{{ action('QuoteController@edit', $quote->id) }}"> <i
                                        class="glyphicon glyphicon-pencil"></i>
                            </a>
                            <a href="{{ action('QuoteController@deleteHandle', $quote->id) }}"> <i
                                        class="glyphicon glyphicon-remove"></i>
                            </a>
                        </div>
                    </blockquote>
                    @endforeach

                    @endif

                            <!-- end repeat blockquotes -->
        </div>
        <!-- end main content -->

        <!-- sidebar -->
        <div class="col-md-4 sidebar">
            <h2>Add quote</h2>

            {!! Form::open(['action' => 'QuoteController@createHandle']) !!}

            @include('quotes.form', ['submittext'=>'Toevoegen'])

            {!! Form::close() !!}
        </div>
        <!-- end sidebar -->

    </div>
    <!-- end content -->

@stop